package config

import (
	"flag"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

var (
	// RestConfig holds the rest config for kubernetes, This is a global config, should be readonly
	// after it is init by main
	RestConfig rest.Config

	// Default qps and burst for rest.Config is pretty low for our use case
	QPS   int
	Burst int

	// used for labels
	BaseDomain string
	// Project controller concurrency worker
	ProjectWorkers int
	// Application controller concurrency worker
	ApplicationWorkers int
	// Period for resource under application to set ownerreference
	OwnerSetInterval int64
)

//TODO: remove this old config
//Options contains all the options for captain
type Options struct {
	// Options contains some useful options
	manager.Options

	// MasterURL is the url of kubernetes apiserver
	MasterURL string
	// KubeConfig is the path for kubeconfig file
	KubeConfig string

	// PrintVersion print the version and exist
	PrintVersion bool
}

// BindFlags init flags and options
func (opt *Options) BindFlags() {

	flag.StringVar(&opt.KubeConfig, "kubeconfig", "",
		"Path to a kubeconfig. Only required if out-of-cluster.")
	flag.StringVar(&opt.MasterURL, "master", "",
		"The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	flag.BoolVar(&opt.PrintVersion, "version", false,
		"Print version")
}
