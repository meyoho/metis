package webhooks

import (
	"alauda.io/metis/pkg/config"
	"alauda.io/metis/pkg/webhooks/labels"
	"alauda.io/metis/pkg/webhooks/resourceratio"
	"context"
	"encoding/base64"
	"fmt"
	"k8s.io/api/admissionregistration/v1beta1"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

var (
	log = ctrl.Log.WithName("webhook")
)

var (
	//ResourceRatio modify pods resources by resource ratio
	ResourceRatio = "resource-ratio"

	//LabelUser add user info for a pod controller
	LabelUser = "label-user"
)

// getPath generate path for a handler. The leading / is mandatory...
func getPath(name string) string {
	return "/" + name
}

//RegisterWebhooks register all the webhook handlers
func RegisterWebhooks(mgr manager.Manager) {
	ws := mgr.GetWebhookServer()

	ws.Register(getPath(ResourceRatio), resourceratio.GetHandler(ResourceRatio))
	ws.Register(getPath(LabelUser), labels.GetHandler(LabelUser))
}

// CreateMutateWebhook create webhook for metis. This is impl in code is because all kinds of bugs caused by
// the pod webhook ,which will block the creation of metis pod
// since cache may be not started yet, we have use a direct client
func CreateMutateWebhook(ns string, caBundle []byte, mgr manager.Manager) error {
	mc := v1beta1.MutatingWebhookConfiguration{}

	log.Info("get kubernetes namespace", "ns", ns)

	decoded, err := base64.StdEncoding.DecodeString(string(caBundle))
	if err != nil {
		return err
	}

	labelUserPath := "/" + LabelUser
	failurePolicy := v1beta1.Ignore
	sideEffects := v1beta1.SideEffectClassNoneOnDryRun

	// use namespace selector to filter out system namespace
	namespaceSelector := metav1.LabelSelector{
		MatchExpressions: []metav1.LabelSelectorRequirement{
			{
				Key:      fmt.Sprintf("%s/project", config.BaseDomain),
				Operator: metav1.LabelSelectorOpNotIn,
				Values: []string{
					"system",
				},
			},
		},
	}

	labelUserWebhook := v1beta1.MutatingWebhook{
		ClientConfig: v1beta1.WebhookClientConfig{
			CABundle: decoded,
			Service: &v1beta1.ServiceReference{
				Name:      "metis-webhook",
				Namespace: ns,
				Path:      &labelUserPath,
			},
		},
		FailurePolicy:     &failurePolicy,
		NamespaceSelector: &namespaceSelector,
		SideEffects:       &sideEffects,
		Name:              "label-user-core.app." + config.BaseDomain,
		Rules: []v1beta1.RuleWithOperations{
			{
				Rule: v1beta1.Rule{
					APIGroups:   []string{""},
					APIVersions: []string{"*"},
					Resources:   []string{"pods", "namespaces", "services", "secrets", "persistentvolumeclaims"},
				},
				Operations: []v1beta1.OperationType{v1beta1.Create, v1beta1.Update},
			},
		},
	}

	labelUserWebhookOthers := v1beta1.MutatingWebhook{
		ClientConfig: v1beta1.WebhookClientConfig{
			CABundle: decoded,
			Service: &v1beta1.ServiceReference{
				Name:      "metis-webhook",
				Namespace: ns,
				Path:      &labelUserPath,
			},
		},
		FailurePolicy:     &failurePolicy,
		NamespaceSelector: &namespaceSelector,
		SideEffects:       &sideEffects,
		Name:              "label-user.app." + config.BaseDomain,
		Rules: []v1beta1.RuleWithOperations{
			{
				Rule: v1beta1.Rule{
					APIGroups:   []string{"apps", "extensions", "app.k8s.io", "batch", "aiops.alauda.io", "monitoring.coreos.com", "auth.alauda.io", "clusterregistry.k8s.io", "kubeovn.io"},
					APIVersions: []string{"*"},
					Resources:   []string{"*"},
				},
				Operations: []v1beta1.OperationType{v1beta1.Create, v1beta1.Update},
			},
		},
	}

	rrPath := "/" + ResourceRatio

	resourceRatioWebhook := v1beta1.MutatingWebhook{
		ClientConfig: v1beta1.WebhookClientConfig{
			CABundle: decoded,
			Service: &v1beta1.ServiceReference{
				Name:      "metis-webhook",
				Namespace: ns,
				Path:      &rrPath,
			},
		},
		FailurePolicy:     &failurePolicy,
		NamespaceSelector: &namespaceSelector,
		SideEffects:       &sideEffects,
		Name:              "resource-ratio.app." + config.BaseDomain,
		Rules: []v1beta1.RuleWithOperations{
			{
				Rule: v1beta1.Rule{
					APIGroups:   []string{""},
					APIVersions: []string{"v1"},
					Resources:   []string{"pods"},
				},
				Operations: []v1beta1.OperationType{v1beta1.Create, v1beta1.Update},
			},
		},
	}

	mc.Webhooks = []v1beta1.MutatingWebhook{
		labelUserWebhook,
		labelUserWebhookOthers,
		resourceRatioWebhook,
	}
	// mc.Annotations = map[string]string{
	// 	"certmanager.k8s.io/inject-ca-from": ns + "/metis-serving-cert",
	// }
	mc.Name = "metis"

	client, err := kubernetes.NewForConfig(mgr.GetConfig())
	if err != nil {
		log.Error(err, "create client for webhook error")
		return err
	}

	if _, err := client.AdmissionregistrationV1beta1().MutatingWebhookConfigurations().Create(&mc); err != nil {
		if isAlreadyExist(err) {
			log.Info("mutate webhook metis alredy exist when create, update it")
			old, err := client.AdmissionregistrationV1beta1().MutatingWebhookConfigurations().Get(mc.GetName(), metav1.GetOptions{})
			if err != nil {
				return err
			}
			mc.SetResourceVersion(old.GetResourceVersion())
			_, err = client.AdmissionregistrationV1beta1().MutatingWebhookConfigurations().Update(&mc)
			return err

		}
		return err
	}

	log.Info("create mutate webhook config metis")

	return nil

}

// DeleteWebhook delete the webhook when metis stop running.
func DeleteWebhook(mgr manager.Manager) error {

	client := mgr.GetClient()
	ctx := context.Background()

	mc := v1beta1.MutatingWebhookConfiguration{}
	mc.Name = "metis"

	if err := client.Delete(ctx, &mc); err != nil {
		if apierrs.IsNotFound(err) {
			log.Info("mutate webhook metis not exist when delete, ignore it")
			return nil
		}
		return err
	}
	log.Info("delete mutate webhook config metis")
	return nil

}

func isAlreadyExist(err error) bool {
	return apierrs.IsAlreadyExists(err)
}
