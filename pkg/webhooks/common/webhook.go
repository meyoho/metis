package common

import (
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// Webhook represents each individual webhook.
type Webhook struct {
	admission.Webhook

	// Decoder is all the code rewriting about
	Decoder Decoder

	// DecoderGenerator helps rewrite the Decoder
	DecoderGenerator DecoderGenerator
}

// GetDecoder returns a decoder to decode the objects embedded in admission requests.
// It may be nil if we haven't received a scheme to use to determine object types yet.
func (w *Webhook) GetDecoder() Decoder {
	return w.Decoder
}

// InjectScheme injects a scheme into the webhook, in order to construct a Decoder.
func (w *Webhook) InjectScheme(s *runtime.Scheme) error {
	// TODO(directxman12): we should have a better way to pass this down

	var err error
	w.Decoder, err = w.DecoderGenerator.NewDecoder(s)
	if err != nil {
		return err
	}

	// inject the decoder here too, just in case the order of calling this is not
	// scheme first, then inject func
	if w.Handler != nil {
		if _, err := InjectDecoderInto(w.GetDecoder(), w.Handler); err != nil {
			return err
		}
	}

	return nil
}
