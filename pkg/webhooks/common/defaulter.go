package common

import (
	"context"
	"encoding/json"
	"net/http"

	"k8s.io/klog"

	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// DefaultingWebhookFor creates a new Webhook for Defaulting the provided type.
func DefaultingWebhookFor(defaulter admission.Defaulter) *Webhook {
	w := admission.Webhook{
		Handler: &mutatingHandler{defaulter: defaulter},
	}
	return &Webhook{Webhook: w}
}

type mutatingHandler struct {
	defaulter admission.Defaulter
	decoder   Decoder
}

var _ DecoderInjector = &mutatingHandler{}

// InjectDecoder injects the decoder into a mutatingHandler.
func (h *mutatingHandler) InjectDecoder(d Decoder) error {
	h.decoder = d
	return nil
}

// Handle handles admission requests.
func (h *mutatingHandler) Handle(ctx context.Context, req admission.Request) admission.Response {
	if h.defaulter == nil {
		panic("defaulter should never be nil")
	}

	// Get the object in the request
	obj := h.defaulter.DeepCopyObject().(admission.Defaulter)
	err := h.decoder.Decode(req, obj)
	if err != nil {
		klog.Error("decode admission request error:", err)
		return admission.Errored(http.StatusBadRequest, err)
	}

	// Default the object
	obj.Default()

	marshalled, err := json.Marshal(obj)
	if err != nil {
		klog.Error("marshal mutated object error:", err)
		return admission.Errored(http.StatusInternalServerError, err)
	}

	// Create the patch
	return admission.PatchResponseFromRaw(req.Object.Raw, marshalled)
}
