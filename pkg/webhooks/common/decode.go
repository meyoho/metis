package common

import (
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

//Decoder is similar to the struct in controller-runtime, but it's a interface
// so we can inject it. Don't known why controller-runtime do this...
type Decoder interface {
	// Decode decodes the inlined object in the AdmissionRequest into the passed-in runtime.Object.
	// If you want decode the OldObject in the AdmissionRequest, use DecodeRaw.
	Decode(req admission.Request, into runtime.Object) error
	// DecodeRaw decodes a RawExtension object into the passed-in runtime.Object.
	DecodeRaw(rawObj runtime.RawExtension, into runtime.Object) error
}

// DecoderInjector is used by the ControllerManager to inject decoder into webhook handlers.
type DecoderInjector interface {
	InjectDecoder(Decoder) error
}

//DecoderGenerator create a Decoder from scheme
type DecoderGenerator interface {
	NewDecoder(scheme *runtime.Scheme) (Decoder, error)
}

// HandlerFunc implements Handler interface using a single function.
type GenerateDecoderFunc func(*runtime.Scheme) (Decoder, error)

var _ DecoderGenerator = GenerateDecoderFunc(nil)

func (f GenerateDecoderFunc) NewDecoder(scheme *runtime.Scheme) (Decoder, error) {
	return f(scheme)
}

// InjectDecoderInto will set decoder on i and return the result if it implements Decoder.  Returns
// false if i does not implement Decoder.
func InjectDecoderInto(decoder Decoder, i interface{}) (bool, error) {
	if s, ok := i.(DecoderInjector); ok {
		return true, s.InjectDecoder(decoder)
	}
	return false, nil
}
