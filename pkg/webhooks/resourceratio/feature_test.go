package resourceratio

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"alauda.io/metis/pkg/config"
	"github.com/gsamokovarov/assert"
)

func TestGetResourceRatio(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			w.WriteHeader(404)
			//fmt.Fprintln(w, "Hello, client")
			// w.WriteHeader(404)
		}))
	defer ts.Close()
	config.RestConfig.Host = ts.URL

	result, err := GetResourceRatio()
	assert.Nil(t, err)
	assert.True(t, result.IsCPUEmpty())
	assert.True(t, result.IsMemEmpty())

	// 200
	ts = httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(200)
			w.Header().Set("Content-Type", "application/json")
			data, err := ioutil.ReadFile("../../../tests/fixtures/feature.json")
			if err != nil {
				panic(err)
			}
			w.Write(data)
		}))
	config.RestConfig.Host = ts.URL
	result, err = GetResourceRatio()
	assert.Nil(t, err)
	assert.Equal(t, 4, result.CPU)
	assert.Equal(t, 3, result.Memory)
}
