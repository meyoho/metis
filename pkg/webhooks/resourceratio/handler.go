package resourceratio

import (
	"net/http"

	"alauda.io/metis/pkg/webhooks/common"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

//GetHandler get handler by name
func GetHandler(name string) http.Handler {
	handler := admission.DefaultingWebhookFor(&Pod{})
	common.InjectLogger(handler, name)
	return handler
}
