package resourceratio

import (
	"os"
	"testing"

	"github.com/gsamokovarov/assert"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/klog"
)

type ss string

func (s ss) String() string {
	return string(s)
}

func TestGetNumberAndUnit(t *testing.T) {
	t.Parallel() // marks TLog as capable of running in parallel with other tests
	tests := []struct {
		name ss
		unit string
	}{
		{"155Mi", "Mi"},
		{"155", ""},
		{"155M", "M"},
	}
	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name.String(), func(t *testing.T) {
			t.Parallel() // marks each test case as capable of running in parallel with each other
			n, u, err := getNumberAndUnit(tt.name)
			assert.Equal(t, n, "155")
			assert.Nil(t, err)
			assert.Equal(t, u, tt.unit)
		})
	}
}

func TestGetCalValue(t *testing.T) {
	t.Parallel()
	klog.SetOutput(os.Stdout)

	tests := []struct {
		name ss

		rr  *ResourceRatio
		rn  v1.ResourceName
		q   resource.Quantity
		err error
	}{
		{"156Mi", &ResourceRatio{4, 3}, v1.ResourceMemory, resource.MustParse("52Mi"), nil},
		{"1300m", &ResourceRatio{4, 3}, v1.ResourceCPU, resource.MustParse("325m"), nil},
		{"2", &ResourceRatio{4, 9}, v1.ResourceCPU, resource.MustParse("500m"), nil},
		{"155Mi", &ResourceRatio{4, 1000}, v1.ResourceMemory, resource.MustParse("159Ki"), nil},
		{"154Mi", &ResourceRatio{4, 3}, v1.ResourceMemory, resource.MustParse("51Mi"), nil},
		{"350Mi", &ResourceRatio{4.5, 4.5}, v1.ResourceMemory, resource.MustParse("78Mi"), nil},
		{"3", &ResourceRatio{4.5, 4.5}, v1.ResourceCPU, resource.MustParse("667m"), nil},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name.String(), func(t *testing.T) {
			t.Parallel()

			q, err := getCalValue(tt.name, tt.rr, tt.rn)
			assert.Nil(t, err)
			assert.Equal(t, q, tt.q)
		})
	}
}
