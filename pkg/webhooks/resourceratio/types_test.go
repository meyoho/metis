package resourceratio

import (
	"alauda.io/warpgate"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"alauda.io/metis/pkg/config"
	"github.com/gsamokovarov/assert"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/klog"
)

func loadPod(path string) *v1.Pod {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data v1.Pod
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

// just for test
//TODO: wait for lib to support this
type fakeFeatureGate struct {
}

func (f *fakeFeatureGate) ListFeatureGates() (fglist *warpgate.AlaudaFeatureGateList, err error) {
	panic("implement me")
}

func (f *fakeFeatureGate) GetFeatureGate(name string) (fg *warpgate.AlaudaFeatureGate, err error) {
	obj := warpgate.AlaudaFeatureGate{}
	obj.Spec.Enabled = true
	return &obj, nil
}

func (f *fakeFeatureGate) IsFeatureGateEnabled(name string) (enabled bool, err error) {
	return true, nil
}

func (f *fakeFeatureGate) ListClusterFeatureGates(cluster string) (fglist *warpgate.AlaudaFeatureGateList, err error) {
	panic("implement me")
}

func (f *fakeFeatureGate) GetClusterFeatureGate(cluster, name string) (fg *warpgate.AlaudaFeatureGate, err error) {
	panic("implement me")
}

func (f *fakeFeatureGate) IsClusterFeatureGateEnabled(cluster, name string) (enabled bool, err error) {
	panic("implement me")
}

func (f *fakeFeatureGate) IsProductLicensed(product string) (enabled bool) {
	panic("implement me")
}

func TestPodResourceRatio(t *testing.T) {

	featureGate = &fakeFeatureGate{}

	p := Pod{*loadPod("../../../tests/fixtures/object.json")}
	klog.SetOutput(os.Stdout)
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(200)
			w.Header().Set("Content-Type", "application/json")
			data, err := ioutil.ReadFile("../../../tests/fixtures/feature.json")
			if err != nil {
				panic(err)
			}
			w.Write(data)
		}))
	config.RestConfig.Host = ts.URL
	r := p.Spec.Containers[0].Resources

	// no change
	p.Default()
	assert.Equal(t, r.Requests.Memory().String(), "256Mi")
	assert.Equal(t, r.Requests.Cpu().String(), "200m")

	// limits cpu
	r.Limits = make(map[v1.ResourceName]resource.Quantity)
	r.Limits[v1.ResourceCPU] = resource.MustParse("1")
	r.Limits[v1.ResourceMemory] = resource.MustParse("1Gi")
	p.Spec.Containers[0].Resources = r
	p.Default()
	assert.Equal(t, r.Requests.Cpu().String(), "250m")
	assert.Equal(t, r.Requests.Memory().String(), "341Mi")

}
