package resourceratio

import (
	"fmt"
	"math"
	"regexp"

	"github.com/pkg/errors"
	"github.com/spf13/cast"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/klog"
)

// getNumberAndUnit a simple parser to split number and unit from Quantity
// example: 155Mi -> 155 Mi
func getNumberAndUnit(r fmt.Stringer) (string, string, error) {
	re := regexp.MustCompile("[0-9]+")
	split := re.FindAllString(r.String(), -1)
	if split == nil {
		return "", "", errors.Errorf("parse resource error: %s", r.String())
	}
	ns := split[0]
	unit := r.String()[len(ns):]
	return ns, unit, nil

}

// QuantityTrans is a map for quantity trans
var QuantityTrans = []struct {
	inputUnit  string
	targetUnit string
	ratio      float64
}{
	{"Ti", "Mi", 1024 * 1024},
	{"T", "M", 1000 * 1000},
	{"Gi", "Mi", 1024},
	{"G", "M", 1000},
	{"Mi", "Ki", 1024},
	{"M", "K", 1000},
	{"Ki", "Ki", 1},
	{"K", "K", 1},
	{"", "m", 1000},
}

// getCalValue cal new Quantity based on old and resource ratio
func getCalValue(old fmt.Stringer, rr *ResourceRatio, rn v1.ResourceName) (resource.Quantity, error) {

	ns, unit, err := getNumberAndUnit(old)
	if err != nil {
		return resource.Quantity{}, err
	}
	ratio := rr.CPU
	if rn != v1.ResourceCPU {
		ratio = rr.Memory
	}

	value := cast.ToFloat64(ns) / cast.ToFloat64(ratio)

	// temp solution: round for Ki/Mi/Gi, not a good idea for now...
	if value < 1.0 {
		for _, item := range QuantityTrans {
			if unit == item.inputUnit {
				unit = item.targetUnit
				value = math.Round(value * item.ratio)
				break

			}
		}
	} else {
		value = math.Round(value)
	}

	input := cast.ToString(value) + unit

	klog.Infof("generate new quantity from: %s ratio: %f", input, ratio)
	result, err := resource.ParseQuantity(input)
	return result, err
}
