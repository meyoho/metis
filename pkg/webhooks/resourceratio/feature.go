package resourceratio

import (
	"crypto/tls"
	"fmt"
	"net/http"

	"alauda.io/metis/pkg/config"
	"github.com/dghubble/sling"
	"k8s.io/klog"
)

var (
	// fatureName is the default resourceratio Feature name
	featureName = "resourceratio"
)

// FeatureSpec...
type FeatureSpec struct {
	Version    string     `json:"version"`
	DeployInfo DeployInfo `json:"deployInfo"`
}

// DeployInfo ...
type DeployInfo struct {
	CPU    float64 `json:"cpu"`
	Memory float64 `json:"memory"`
}

//ResourceRatio...
type ResourceRatio DeployInfo

// IsCPUEmpty ...
// we need `<` because some struct bind can create nil value
func (ratio *ResourceRatio) IsCPUEmpty() bool {
	return ratio.CPU <= 1
}

// IsMemEmpty ...
func (ratio *ResourceRatio) IsMemEmpty() bool {
	return ratio.Memory <= 1
}

// Feature is the definition of Feature crd
type Feature struct {
	Spec FeatureSpec `json:"spec"`
}

//GetResourceRatio get resourceratio info from the Featrure resource
// If not found, return the default one.
// We should use some cache in the feature
func GetResourceRatio() (*ResourceRatio, error) {
	base := config.RestConfig.Host
	path := "apis/infrastructure.alauda.io/v1alpha1/features"
	url := fmt.Sprintf("%s/%s/%s", base, path, featureName)
	var feature Feature

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	resp, err := sling.New().
		Client(client).
		Set("Authorization", "Bearer "+config.RestConfig.BearerToken).
		Get(url).ReceiveSuccess(&feature)
	if err != nil {
		klog.Error("get feature resource error:", err)
		return nil, err
	}
	if resp.StatusCode == 404 {
		klog.Warning("resource ratio feature not found, return default value")
		return &ResourceRatio{CPU: 1, Memory: 1}, nil
	}
	result := ResourceRatio(feature.Spec.DeployInfo)
	return &result, nil

}
