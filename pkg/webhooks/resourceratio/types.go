package resourceratio

import (
	wg "alauda.io/warpgate"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
)

var (
	podlog = ctrl.Log.WithName("pod-webhook")

	// controls whether this webhook will run or not
	featureGate wg.WarpGate
)

// Pod is a wrapper to act as a Defaulter()

// +kubebuilder:webhook:path=/resource-ratio,mutating=true,failurePolicy=ignore,groups=core,resources=pods,verbs=create;update,versions=v1,name=mpod.kb.io

type Pod struct {
	v1.Pod
}

// Default modify pod's resource
func (p *Pod) Default() {

	// In test env, we can create a fake var to skip this part
	if featureGate == nil {
		wwg, err := wg.NewLocalWarpGate()
		if err != nil {
			podlog.Error(err, "init feature gate error")
			panic(err)
		}
		featureGate = wwg
	}

	// debug info
	fg, err := featureGate.GetFeatureGate("resource-ratio")
	podlog.Info("get resource ratio feature gate", "data", fg, "err", err)

	flag, err := featureGate.IsFeatureGateEnabled("resource-ratio")
	if err != nil {
		podlog.Error(err, "check resource ratio feature gate error")
		return
	}

	if !flag {
		podlog.Info("resource ratio feature gate is disabled")
		return

	}

	podlog.Info("hi....", "name", p.GetName())

	ratio, err := GetResourceRatio()
	if err != nil {
		podlog.Error(err, "get resource ratio error when mutate pod:")
		return
	}

	podlog.Info("pod info", "resources", p.Spec.Containers[0].Resources, "ratio", ratio)

	spec := p.Spec
	var cs []v1.Container

	for _, ct := range spec.Containers {
		if ct.Resources.Limits != nil {
			if !ct.Resources.Limits.Cpu().IsZero() && !ratio.IsCPUEmpty() {
				if ct.Resources.Requests == nil {
					ct.Resources.Requests = make(map[v1.ResourceName]resource.Quantity)
				}
				result, err := getCalValue(ct.Resources.Limits.Cpu(), ratio, v1.ResourceCPU)
				if err != nil {
					podlog.Error(err, "get cal values for cpu error:")
					return
				}
				podlog.Info("rewrite container cpu requests", "name", ct.Name, "result", result.String())
				ct.Resources.Requests[v1.ResourceCPU] = result
			}
			if !ct.Resources.Limits.Memory().IsZero() && !ratio.IsMemEmpty() {
				if ct.Resources.Requests == nil {
					ct.Resources.Requests = make(map[v1.ResourceName]resource.Quantity)
				}
				result, err := getCalValue(ct.Resources.Limits.Memory(), ratio, v1.ResourceMemory)
				if err != nil {
					podlog.Error(err, "get cal values for mem error:")
					return
				}
				podlog.Info("rewrite container mem requests", "name", ct.Name, "result", result.String())
				ct.Resources.Requests[v1.ResourceMemory] = result
			}
		}

		cs = append(cs, ct)
	}
	p.Spec.Containers = cs

}

func (p *Pod) DeepCopyObject() runtime.Object {
	o := p.Pod.DeepCopyObject().(*v1.Pod)
	return &Pod{
		*o,
	}
}
