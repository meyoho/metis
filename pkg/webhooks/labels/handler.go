package labels

import (
	"net/http"

	"alauda.io/metis/pkg/webhooks/common"
)

//GetHandler get a handler by name
func GetHandler(name string) http.Handler {
	handler := common.DefaultingWebhookFor(&Resource{})
	handler.DecoderGenerator = common.GenerateDecoderFunc(newDecoder)
	common.InjectLogger(handler, name)
	return handler
}
