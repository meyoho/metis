package labels

import (
	"encoding/json"
	"strings"

	"alauda.io/metis/pkg/config"
	"alauda.io/metis/pkg/webhooks/common"

	"github.com/thoas/go-funk"
	"k8s.io/api/admission/v1beta1"
	"k8s.io/apimachinery/pkg/api/meta"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

var (
	log = ctrl.Log.WithName("label-user")
)

type decoder struct {
	admission.Decoder
}

func newDecoder(scheme *runtime.Scheme) (common.Decoder, error) {
	d, err := admission.NewDecoder(scheme)
	if err != nil {
		return nil, err
	}
	return &decoder{
		*d,
	}, nil
}

//Decode read user info from the admission request and add to the runtime object in the annotations
func (d *decoder) Decode(req admission.Request, into runtime.Object) error {
	log.Info("get user info from admission request:", "user", req.UserInfo)

	err := d.DecodeRaw(req.Object, into)
	if err != nil {
		return err
	}

	if strings.Contains(req.UserInfo.Username, ":") {
		log.Info("user info in admission request contains invalid label char, may be a system user, ignore it:", "username", req.UserInfo.Username)
		return nil
	}

	ac, err := meta.Accessor(into)
	if err != nil {
		log.Error(err, "get meta accessor error for object")
		return err
	}

	// some temp resources does not have mta
	if ac.GetName() == "" {
		log.Info("resource name is empty, skip add creator annotation")
		return nil
	}

	ano := ac.GetAnnotations()
	if ano == nil {
		ano = make(map[string]string)
	}

	//Ignore Operation Update,Delete
	if req.Operation == v1beta1.Create {
		ano[config.BaseDomain+"/creator"] = req.UserInfo.Username
	}

	ano[config.BaseDomain+"/updated-at"], _ = v1.Now().MarshalQueryParameter()
	ac.SetAnnotations(ano)

	log.Info("set annotations for resource", "annotations", ano)

	if isPodControllerKind(req.Kind.Kind) && req.Operation == v1beta1.Create {
		log.Info("set user info annotations for podController")
		podAno := map[string]string{
			config.BaseDomain + "/creator": req.UserInfo.Username,
		}
		if unstructuredInto, isUnstructured := into.(*Resource); isUnstructured {
			err := setPodControllerTemplateAnnotations(unstructuredInto, podAno)
			if err != nil {
				log.Error(err, "set user info annotations for podController")
				return err
			}
		}
	}

	return nil

}

// DecodeRaw decodes a RawExtension object into the passed-in runtime.Object.
// We only support Resource here...
func (d *decoder) DecodeRaw(rawObj runtime.RawExtension, into runtime.Object) error {
	// NB(directxman12): there's a bug/weird interaction between decoders and
	// the API server where the API server doesn't send a GVK on the embedded
	// objects, which means the unstructured decoder refuses to decode.  It
	// also means we can't pass the unstructured directly in, since it'll try
	// and call unstructured's special Unmarshal implementation, which calls
	// back into that same decoder :-/
	// See kubernetes/kubernetes#74373.
	if unstructuredInto, isUnstructured := into.(*Resource); isUnstructured {
		// unmarshal into unstructured's underlying object to avoid calling the decoder
		if err := json.Unmarshal(rawObj.Raw, &unstructuredInto.Object); err != nil {
			return err
		}
	}
	return nil
}

var podControllerKind = []string{
	"Deployment",
	"DaemonSet",
	"StatefulSet",
	"Job",
	"CronJob",
}

var podControllerAnnotationFieldPath = []string{
	"spec", "template", "metadata", "annotations",
}
var podControllerCronJobAnnotationFieldPath = []string{
	"spec", "jobTemplate",
	"spec", "template", "metadata", "annotations",
}

func isPodControllerKind(kind string) bool {
	return funk.Contains(podControllerKind, kind)
}

func setPodControllerTemplateAnnotations(r *Resource, userInfo map[string]string) error {
	var annotations map[string]interface{}
	var filedPath []string
	switch r.GetKind() {
	case "CronJob":
		filedPath = podControllerCronJobAnnotationFieldPath
		break
	default:
		filedPath = podControllerAnnotationFieldPath
	}

	annotations, ok, err := unstructured.NestedMap(r.Object, filedPath...)
	if err != nil || !ok {
		annotations = map[string]interface{}{}
	}

	for k, v := range userInfo {
		annotations[k] = v
	}

	err = unstructured.SetNestedMap(r.Object, annotations, filedPath...)
	if err != nil {
		return err
	}
	return nil
}
