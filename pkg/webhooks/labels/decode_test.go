package labels

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"alauda.io/metis/pkg/config"

	"github.com/gsamokovarov/assert"
	"k8s.io/api/admission/v1beta1"
	admissionv1beta1 "k8s.io/api/admission/v1beta1"
	authenticationv1 "k8s.io/api/authentication/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

func TestMetaTime(t *testing.T) {
	tm, _ := metav1.Now().MarshalQueryParameter()
	t.Log("time:", tm)

}

func getResourceByKind(kind string) string {
	switch kind {
	case "Pod":
		return "pods"
	case "Deployment":
		return "deployments"
	case "CronJob":
		return "cronjobs"
	default:
		return ""
	}
}
func loadPod(path string) *unstructured.Unstructured {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data unstructured.Unstructured
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

func getReq(data *unstructured.Unstructured) *admission.Request {
	bt, _ := json.Marshal(data)
	gvk := data.GroupVersionKind()
	return &admission.Request{
		AdmissionRequest: admissionv1beta1.AdmissionRequest{
			UID:       data.GetUID(),
			Operation: v1beta1.Create,
			UserInfo: authenticationv1.UserInfo{
				Username: "tom",
				UID:      "",
				Groups:   nil,
				Extra:    nil,
			},
			Kind: metav1.GroupVersionKind{
				Group:   gvk.Group,
				Version: gvk.Version,
				Kind:    gvk.Kind,
			},
			Resource: metav1.GroupVersionResource{
				Group:    gvk.Group,
				Version:  gvk.Version,
				Resource: getResourceByKind(gvk.Kind),
			},
			Object: runtime.RawExtension{
				Raw:    bt,
				Object: data,
			},
		},
	}
}

func conditionPodAnnotation(r Resource, annKey, annValue string) bool {
	var path []string
	switch r.GetKind() {
	case "CronJob":
		path = []string{
			"spec", "jobTemplate",
			"spec", "template", "metadata", "annotations",
		}
		break
	default:
		path = []string{
			"spec", "template", "metadata", "annotations",
		}
		break
	}
	ann, ok, err := unstructured.NestedMap(r.Object, path...)
	if err != nil || !ok {
		return false
	}

	return ann[annKey] == annValue
}

func TestDecode(t *testing.T) {
	data := loadPod("../../../tests/fixtures/object.json")
	klog.SetOutput(os.Stdout)
	config.BaseDomain = "alauda.io"

	d, err := newDecoder(nil)
	assert.Nil(t, err)

	req := getReq(data)
	var r Resource
	err = d.Decode(*req, &r)
	assert.Nil(t, err)

	t.Log(r)

	assert.Equal(t, r.GetAnnotations()["alauda.io/creator"], "tom")
	assert.NotEqual(t, r.GetAnnotations()["alauda.io/updated-at"], "")
}

func TestDecoderForPodController(t *testing.T) {
	var createCases = []string{
		"../../../tests/fixtures/deployment.json",
		"../../../tests/fixtures/cronjob.json",
	}
	var updateCases = []string{
		"../../../tests/fixtures/deployment-update.json",
		"../../../tests/fixtures/cronjob-update.json",
	}
	klog.SetOutput(os.Stdout)
	config.BaseDomain = "alauda.io"

	d, err := newDecoder(nil)
	assert.Nil(t, err)

	for _, path := range createCases {
		t.Log(path)
		data := loadPod(path)
		req := getReq(data)
		req.Operation = v1beta1.Create
		var r Resource
		err = d.Decode(*req, &r)
		assert.Nil(t, err)

		t.Log(r)
		assert.True(t, conditionPodAnnotation(r, "alauda.io/creator", "tom"))
	}
	for _, path := range updateCases {
		t.Log(path)
		data := loadPod(path)
		req := getReq(data)
		req.Operation = v1beta1.Update
		var r Resource
		err = d.Decode(*req, &r)
		assert.Nil(t, err)

		t.Log(r)
		assert.True(t, conditionPodAnnotation(r, "alauda.io/creator", "aiyijing"))
	}
}
