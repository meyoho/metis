package labels

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
)

// Pod is a wrapper to act as a Defaulter()
// +kubebuilder:webhook:path=/label-user,mutating=true,failurePolicy=ignore,groups=core,resources=pods,verbs=create;update,versions=v1,name=label-user.kb.io

type Resource struct {
	unstructured.Unstructured
}

// Default do nothing... The part that matters is in Decode
func (w *Resource) Default() {
	log.Info("hi....i do nothing...", "name", w.GetName())
}

//DeepCopyObject ....
func (w *Resource) DeepCopyObject() runtime.Object {
	o := w.Unstructured.DeepCopyObject().(*unstructured.Unstructured)
	return &Resource{
		*o,
	}
}
