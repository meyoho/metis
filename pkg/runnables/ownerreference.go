package runnables

import (
	appv1beta1 "alauda.io/metis/apis/app/v1beta1"
	"context"
	"fmt"
	"github.com/google/go-cmp/cmp"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sync"
	"time"
)

var _ manager.LeaderElectionRunnable = &ApplicationOwnerReferenceController{}
var _ manager.Runnable = &ApplicationOwnerReferenceController{}

type ApplicationOwnerReferenceController struct {
	Client            client.Client
	RestConfig        *rest.Config
	Scheme            *runtime.Scheme
	OwnerSetInterval  time.Duration
	resourceTypesLock sync.Mutex
	resourceTypes     map[string]*metav1.APIResource
}

func NewApplicationOwnerReferenceController(mgr manager.Manager, ownerSetInterval int64) (*ApplicationOwnerReferenceController, error) {
	return &ApplicationOwnerReferenceController{
		Client:           mgr.GetClient(),
		RestConfig:       mgr.GetConfig(),
		Scheme:           mgr.GetScheme(),
		OwnerSetInterval: time.Second * time.Duration(ownerSetInterval),
	}, nil
}

func (c *ApplicationOwnerReferenceController) NeedLeaderElection() bool {
	return true
}

func (c *ApplicationOwnerReferenceController) setComponentsOwnerReferenceForAll() {
	ctx := context.Background()
	applications := &appv1beta1.ApplicationList{}
	if err := c.Client.List(ctx, applications); err != nil {
		klog.Error("List applications error:", err)
		return
	}
	for _, app := range applications.Items {
		c.trySetComponentsOwnerReference(&app)
	}
}

func (c *ApplicationOwnerReferenceController) initResourceTypes() (err error) {
	c.resourceTypesLock.Lock()
	defer c.resourceTypesLock.Unlock()
	if c.resourceTypes != nil {
		return
	}
	d, err := discovery.NewDiscoveryClientForConfig(c.RestConfig)
	if err != nil {
		return
	}
	rl, err := d.ServerPreferredResources()
	if err != nil {
		return
	}
	rlmap := make(map[string]*metav1.APIResource)
	for _, r := range rl {
		for _, rr := range r.APIResources {
			if rlmap[rr.Kind] == nil {
				rr := rr
				gv, _ := schema.ParseGroupVersion(r.GroupVersion)
				rr.Group = gv.Group
				rr.Version = gv.Version
				rlmap[rr.Kind] = &rr
			}
		}
	}
	c.resourceTypes = rlmap
	return
}

func (c *ApplicationOwnerReferenceController) groupVersionResourceForKind(kind string) (gvr *schema.GroupVersionResource, namespaced bool, err error) {
	err = c.initResourceTypes()
	if err != nil {
		return
	}
	r := c.resourceTypes[kind]
	if r == nil {
		return nil, false, fmt.Errorf("resource kind %q not found", kind)
	}
	return &schema.GroupVersionResource{
		Group:    r.Group,
		Version:  r.Version,
		Resource: r.Name,
	}, r.Namespaced, nil
}

// trySetComponentsOwnerReference tries to query for the components of the application,
// sets the owner references of them if the owner is null.
func (c *ApplicationOwnerReferenceController) trySetComponentsOwnerReference(app *appv1beta1.Application) {
	klog.V(1).Infof("Try to set OwnerReference for components of application %q", app.GetName())

	for _, o := range app.Spec.ComponentGroupKinds {
		gvr, namespaced, err := c.groupVersionResourceForKind(o.Kind)
		if !namespaced {
			// Skip the non namespaced resource
			continue
		}
		ns := app.GetNamespace()
		if err != nil {
			return
		}
		ri, err := dynamic.NewForConfig(c.RestConfig)
		if err != nil {
			return
		}
		client := ri.Resource(*gvr).Namespace(ns)
		mlabels := app.Spec.Selector.MatchLabels
		if len(mlabels) == 0 {
			err = fmt.Errorf("the Application selector match labels is null")
			klog.Errorf("Application %s.%s selector match labels is null", app.Name, app.Namespace)
			return
		}

		res, err := client.List(metav1.ListOptions{
			LabelSelector: labels.FormatLabels(mlabels),
		})
		if err != nil {
			klog.Errorf("List application resources for %q error: %v", gvr.String(), err)
			return
		}
		for _, o := range res.Items {
			originObj := o.DeepCopy()
			if err := controllerutil.SetControllerReference(app, &o, c.Scheme); err != nil {
				klog.Errorf("Set OwnerReference for %q %q error: %v", gvr.String(), o.GetName(), err)
				continue
			}
			if cmp.Equal(o.GetOwnerReferences(), originObj.GetOwnerReferences()) {
				continue
			}
			klog.V(1).Infof("Set OwnerReference for %q %q", gvr.String(), o.GetName())
			_, err = client.Update(&o, metav1.UpdateOptions{})
			if err != nil {
				klog.Errorf("Set OwnerReference for %q %q error: %v", gvr.String(), o.GetName(), err)
				return
			}
			return
		}
	}
}

func (c *ApplicationOwnerReferenceController) Start(stopCh <-chan struct{}) error {
	klog.Infof("Starting ApplicationOwnerReferenceController with interval %s", c.OwnerSetInterval)
	defer klog.Info("Stopping ApplicationOwnerReferenceController")
	wait.Until(func() {
		c.setComponentsOwnerReferenceForAll()
	}, c.OwnerSetInterval, stopCh)
	return nil
}
