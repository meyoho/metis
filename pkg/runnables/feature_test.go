package runnables

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"alauda.io/metis/pkg/config"
	"github.com/go-logr/zapr"
	"github.com/gsamokovarov/assert"
	"go.uber.org/zap"
)

func TestSyncResourceRatioFeature(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintln(w, "Hello, client")
			// w.WriteHeader(404)
		}))
	defer ts.Close()

	l, _ := zap.NewDevelopment()
	logger = zapr.NewLogger(l)

	httpClient = ts.Client()
	config.RestConfig.Host = ts.URL
	err := ensureResourceRatioFeatureExist()
	assert.Nil(t, err)

	// not exist
	ts = httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == "GET" {
				w.WriteHeader(404)
				fmt.Fprintln(w, "Hello, client")
			} else {
				w.WriteHeader(201)
			}

		}))
	httpClient = ts.Client()
	config.RestConfig.Host = ts.URL
	err = ensureResourceRatioFeatureExist()
	assert.Nil(t, err)

}
