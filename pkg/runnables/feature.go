package runnables

import (
	"alauda.io/metis/pkg/config"
	"crypto/tls"
	"fmt"
	"net/http"
	"strings"

	"github.com/dghubble/sling"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

var logger = log.Log.WithName("rr-runnable")

var defaultData = `{
  "metadata": {
    "name": "resourceratio"
  },
  "spec": {
    "deployInfo": {
      "cpu": 1,
      "memory": 1
    },
    "type": "resourceratio",
    "version": "1.0"
  },
  "apiVersion": "infrastructure.alauda.io/v1alpha1",
  "kind": "Feature"
}`

var (
	tr = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient = &http.Client{Transport: tr}
)

// This ugly function will create resource ratio feature if not exist
func ensureResourceRatioFeatureExist() error {
	base := config.RestConfig.Host
	path := "apis/infrastructure.alauda.io/v1alpha1/features"
	url := fmt.Sprintf("%s/%s/%s", base, path, "resourceratio")

	req, err := sling.New().
		Set("Authorization", "Bearer "+config.RestConfig.BearerToken).
		Get(url).Request()
	if err != nil {
		logger.Error(err, "create first request error")
		return err
	}
	resp, err := httpClient.Do(req)
	if err != nil {
		logger.Error(err, "do request error when check resource ratio")
		return err
	}

	if resp.StatusCode == 200 {
		logger.Info("resourceratio feature already exist")
		return nil
	}

	if resp.StatusCode == 404 {
		logger.Info("default resource ratio feature not exist, create it")
		body := strings.NewReader(defaultData)
		req, err := sling.New().
			Base(base+"/").
			Post(path).
			Body(body).
			Set("Content-Type", "application/json").
			Set("Authorization", "Bearer "+config.RestConfig.BearerToken).
			Request()
		if err != nil {
			logger.Error(err, "create request error when create resource ratio")
			return err
		}
		resp, err := httpClient.Do(req)
		if err != nil {
			logger.Error(err, "do request error when create resource ratio")
			return err
		}
		if resp.StatusCode == 201 {
			logger.Info("create resource ratio successfully")
			return nil
		}
		return fmt.Errorf("create resource ratio error: %d", resp.StatusCode)
	}
	return fmt.Errorf("check resource ratio error, code: %d", resp.StatusCode)
}
