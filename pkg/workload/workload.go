package workload

const (
	DeploymentKind     = "Deployment"
	StatefulSetKind    = "StatefulSet"
	DaemonSetKind      = "DaemonSet"
	PodControllerGroup = "apps"
)

func IsPodController(kind string) bool {
	switch kind {
	case DeploymentKind, DaemonSetKind, StatefulSetKind:
		return true
	default:
		return false
	}
}
