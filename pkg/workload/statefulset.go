package workload

import (
	"alauda.io/metis/apis/app/v1beta1"
	appsv1 "k8s.io/api/apps/v1"
)

// https://bitbucket.org/mathildetech/krobelus/src/298f0188b9acfdac4101de83fe63dc5273c0a8ea/model/application.go#model/application.go-403
func CalculateStatefulsetStatus(statefulset *appsv1.StatefulSet) *v1beta1.Workload {
	wl := v1beta1.Workload{}
	wl.Group = PodControllerGroup
	wl.Kind = StatefulSetKind
	wl.Name = statefulset.Name

	generation := statefulset.GetGeneration()
	current := statefulset.Status.CurrentReplicas
	desired := int32(0)
	if statefulset.Spec.Replicas != nil {
		desired = *statefulset.Spec.Replicas
	}
	observedReplicas := statefulset.Status.Replicas
	readyReplicas := statefulset.Status.ReadyReplicas
	observedGeneration := statefulset.Status.ObservedGeneration
	currentRevision := statefulset.Status.CurrentRevision
	updateRevision := statefulset.Status.UpdateRevision
	updateStrategy := statefulset.Spec.UpdateStrategy

	if desired > 0 && observedReplicas == desired && readyReplicas == desired && observedGeneration >= generation {
		if appsv1.RollingUpdateStatefulSetStrategyType == updateStrategy.Type && (currentRevision != updateRevision || current != desired) {
			//Pending
			wl.Status = v1beta1.WLPending
		} else {
			// Running
			wl.Status = v1beta1.WLRunning
		}
	} else if desired == 0 && observedReplicas == desired && readyReplicas == desired && observedGeneration >= generation {
		if appsv1.RollingUpdateStatefulSetStrategyType == updateStrategy.Type && (currentRevision != updateRevision || current != desired) {
			//Pending
			wl.Status = v1beta1.WLPending
		} else {
			// Stopped
			wl.Status = v1beta1.WLStopped
		}
	} else {
		//Pending
		wl.Status = v1beta1.WLPending
	}

	wl.Current = readyReplicas
	wl.Desired = desired

	messages := []v1beta1.Message{}
	for _, condition := range statefulset.Status.Conditions {
		messages = append(messages, v1beta1.Message{
			Reason:  condition.Reason,
			Message: condition.Message,
		})
		wl.Messages = messages
	}

	return &wl
}
