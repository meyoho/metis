package workload

import (
	"alauda.io/metis/apis/app/v1beta1"
	appsv1 "k8s.io/api/apps/v1"
)

// https://bitbucket.org/mathildetech/krobelus/src/298f0188b9acfdac4101de83fe63dc5273c0a8ea/model/application.go#model/application.go-3130
func CalculateDaemonsetStatus(daemonset *appsv1.DaemonSet) *v1beta1.Workload {
	wl := v1beta1.Workload{}
	wl.Group = PodControllerGroup
	wl.Kind = DaemonSetKind
	wl.Name = daemonset.Name

	generation := daemonset.GetGeneration()
	current := daemonset.Status.NumberAvailable
	numberReady := daemonset.Status.NumberReady
	numberAvailable := daemonset.Status.NumberAvailable
	updatedNumberScheduled := daemonset.Status.UpdatedNumberScheduled
	currentNumberScheduled := daemonset.Status.CurrentNumberScheduled
	desired := daemonset.Status.DesiredNumberScheduled
	observedGeneration := daemonset.Status.ObservedGeneration
	updateStrategy := daemonset.Spec.UpdateStrategy

	if desired > 0 && observedGeneration >= generation && currentNumberScheduled == desired && numberReady == desired && numberAvailable == desired {
		if appsv1.RollingUpdateDaemonSetStrategyType == updateStrategy.Type && updatedNumberScheduled != desired {
			//Pending
			wl.Status = v1beta1.WLPending
		} else {
			// Running
			wl.Status = v1beta1.WLRunning
		}
	} else if desired == 0 &&
		observedGeneration >= generation && numberReady == desired && numberAvailable == desired && currentNumberScheduled == desired {
		if appsv1.RollingUpdateDaemonSetStrategyType == updateStrategy.Type && updatedNumberScheduled != desired {
			//Pending
			wl.Status = v1beta1.WLPending
		} else {
			// Stopped
			wl.Status = v1beta1.WLStopped
		}
	} else {
		//Pending
		wl.Status = v1beta1.WLPending
	}

	wl.Current = current
	wl.Desired = desired

	messages := []v1beta1.Message{}
	for _, condition := range daemonset.Status.Conditions {
		messages = append(messages, v1beta1.Message{
			Reason:  condition.Reason,
			Message: condition.Message,
		})
		wl.Messages = messages
	}

	return &wl
}
