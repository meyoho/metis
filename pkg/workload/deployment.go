package workload

import (
	"alauda.io/metis/apis/app/v1beta1"
	appsv1 "k8s.io/api/apps/v1"
)

// https://bitbucket.org/mathildetech/krobelus/src/298f0188b9acfdac4101de83fe63dc5273c0a8ea/model/application.go#model/application.go-233
func CalculateDeploymentStatus(deployment *appsv1.Deployment) *v1beta1.Workload {
	wl := v1beta1.Workload{}
	wl.Kind = DeploymentKind
	wl.Group = PodControllerGroup
	wl.Name = deployment.Name

	generation := deployment.GetGeneration()
	current := deployment.Status.AvailableReplicas
	desired := int32(0)
	if deployment.Spec.Replicas != nil {
		desired = *deployment.Spec.Replicas
	}
	observedReplicas := deployment.Status.Replicas
	updatedReplicas := deployment.Status.UpdatedReplicas
	observedGeneration := deployment.Status.ObservedGeneration

	if desired > 0 && updatedReplicas == desired && observedReplicas == desired && current == desired && observedGeneration >= generation {
		// Running
		wl.Status = v1beta1.WLRunning
	} else if desired == 0 &&
		observedReplicas == desired && updatedReplicas == desired && current == desired && observedGeneration >= generation {
		// Stopped
		wl.Status = v1beta1.WLStopped
	} else {
		//Pending:
		// Scale Up
		// Scale Down
		// Rolling update
		// Recreate
		// Unknown
		wl.Status = v1beta1.WLPending
	}

	wl.Current = current
	wl.Desired = desired

	messages := []v1beta1.Message{}
	for _, condition := range deployment.Status.Conditions {
		messages = append(messages, v1beta1.Message{
			Reason:  condition.Reason,
			Message: condition.Message,
		})
		wl.Messages = messages
	}

	return &wl
}
