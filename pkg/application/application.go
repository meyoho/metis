package application

import (
	"alauda.io/metis/apis/app/v1beta1"
	"fmt"
	"github.com/google/uuid"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
)

// NewApplicationOwnerRef create a owner ref for sub resources
func NewApplicationOwnerRef(name string, uid types.UID) []metav1.OwnerReference {
	t := true
	owner := metav1.OwnerReference{
		APIVersion:         "app.k8s.io/v1beta1",
		BlockOwnerDeletion: &t,
		Controller:         &t,
		Kind:               "Application",
		Name:               name,
		UID:                uid,
	}
	return []metav1.OwnerReference{owner}
}

func NewHelmRequestOwnerRef(name string, uid types.UID) []metav1.OwnerReference {
	t := true
	owner := metav1.OwnerReference{
		APIVersion:         "app.alauda.io/v1alpha1",
		BlockOwnerDeletion: &t,
		Controller:         &t,
		Kind:               "HelmRequest",
		Name:               name,
		UID:                uid,
	}
	return []metav1.OwnerReference{owner}
}

// NewApplication create a new application resource
// applications have the same name and namespace as helmrequest
func NewApplication(name, namespace, release string, rs []unstructured.Unstructured) *v1beta1.Application {
	var app v1beta1.Application
	app.Name = name
	app.Namespace = namespace

	uid := uuid.New().String()
	app.SetLabels(map[string]string{
		"app.alauda.io/uuid":    uid,
		"sync-from-helmrequest": "true",
	})

	app.SetAnnotations(map[string]string{
		"captain-release-name": release,
	})

	// spec part
	app.Spec.Selector = &metav1.LabelSelector{}
	app.Spec.Selector.MatchLabels = map[string]string{
		"app.alauda.io/name": fmt.Sprintf("%s.%s", name, namespace),
	}
	app.Spec.AssemblyPhase = v1beta1.Pending

	// gvk
	exist := map[string]bool{}
	for _, item := range rs {
		if exist[item.GroupVersionKind().String()] {
			continue
		}
		app.Spec.ComponentGroupKinds = append(app.Spec.ComponentGroupKinds, metav1.GroupKind{
			Group: item.GroupVersionKind().Group,
			Kind:  item.GetKind(),
		})
		exist[item.GroupVersionKind().String()] = true
	}

	// init status.. This is wierd
	app.Status.WorkloadsStatus.Workloads = []v1beta1.Workload{}

	return &app

}
