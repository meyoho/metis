module alauda.io/metis

go 1.13

require (
	alauda.io/warpgate v0.0.1
	github.com/alauda/component-base v0.0.0-20191209085201-110d478ed32f
	github.com/alauda/helm-crds v0.0.0-20190915014518-6c1be05f7d6e
	github.com/dghubble/sling v1.3.0
	github.com/go-logr/logr v0.1.0
	github.com/go-logr/zapr v0.1.0
	github.com/google/go-cmp v0.3.0
	github.com/google/uuid v1.1.1
	github.com/gsamokovarov/assert v0.0.0-20180414063448-8cd8ab63a335
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/pkg/errors v0.8.1
	github.com/spf13/cast v1.3.0
	github.com/thoas/go-funk v0.5.0
	go.uber.org/zap v1.10.0
	k8s.io/api v0.17.2
	k8s.io/apimachinery v0.17.2
	k8s.io/client-go v11.0.1-0.20190409021438-1a26190bd76a+incompatible
	k8s.io/klog v1.0.0
	k8s.io/kubernetes v1.16.6
	sigs.k8s.io/controller-runtime v0.5.0
)

replace alauda.io/warpgate v0.0.1 => bitbucket.org/mathildetech/warpgate v0.0.9

replace github.com/prometheus/client_golang => github.com/prometheus/client_golang v0.9.2

replace k8s.io/client-go => k8s.io/client-go v0.16.6

replace k8s.io/api => k8s.io/api v0.16.6

replace k8s.io/apiextensions-apiserver => k8s.io/apiextensions-apiserver v0.16.6

replace k8s.io/apimachinery => k8s.io/apimachinery v0.16.7-beta.0

replace k8s.io/apiserver => k8s.io/apiserver v0.16.6

replace k8s.io/cli-runtime => k8s.io/cli-runtime v0.16.6

replace k8s.io/cloud-provider => k8s.io/cloud-provider v0.16.6

replace k8s.io/cluster-bootstrap => k8s.io/cluster-bootstrap v0.16.6

replace k8s.io/code-generator => k8s.io/code-generator v0.16.7-beta.0

replace k8s.io/component-base => k8s.io/component-base v0.16.6

replace k8s.io/cri-api => k8s.io/cri-api v0.16.9-beta.0

replace k8s.io/csi-translation-lib => k8s.io/csi-translation-lib v0.16.6

replace k8s.io/kube-aggregator => k8s.io/kube-aggregator v0.16.6

replace k8s.io/kube-controller-manager => k8s.io/kube-controller-manager v0.16.6

replace k8s.io/kube-proxy => k8s.io/kube-proxy v0.16.6

replace k8s.io/kube-scheduler => k8s.io/kube-scheduler v0.16.6

replace k8s.io/kubectl => k8s.io/kubectl v0.16.6

replace k8s.io/kubelet => k8s.io/kubelet v0.16.6

replace k8s.io/legacy-cloud-providers => k8s.io/legacy-cloud-providers v0.16.6

replace k8s.io/metrics => k8s.io/metrics v0.16.6

replace k8s.io/node-api => k8s.io/node-api v0.16.6

replace k8s.io/sample-apiserver => k8s.io/sample-apiserver v0.16.6

replace k8s.io/sample-cli-plugin => k8s.io/sample-cli-plugin v0.16.6

replace k8s.io/sample-controller => k8s.io/sample-controller v0.16.6
