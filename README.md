# Metis


## Functionality

* Webhook: add creator and update-at for some resources
* Webhook: apply resource ratio to Pod
* Controller: create Application for HelmRequest


## Run Local

### Prerequisite

Kubernetes

kubebuilder(https://book.kubebuilder.io/quick-start.html)


### Commands

`make docker-build && make deploy`


## Test

`make test`
