/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"time"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	quota "k8s.io/kubernetes/pkg/quota/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	authv1 "alauda.io/metis/apis/auth/v1"
)

// ProjectQuotaReconciler reconciles a ProjectQuota object
type ProjectQuotaReconciler struct {
	client.Client
	Log         logr.Logger
	Scheme      *runtime.Scheme
	Domain      string
	Concurrency int
}

// +kubebuilder:rbac:groups=auth.alauda.io,resources=projectquota,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=projectquota/status,verbs=get;update;patch

func (r *ProjectQuotaReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	logger := r.Log.WithValues("projectquota", req.NamespacedName)
	st := time.Now()
	logger.Info("Starting reconcile")
	defer logger.Info("Finish reconcile", "cost", time.Since(st))

	projectquota := &authv1.ProjectQuota{}
	if err := r.Client.Get(ctx, types.NamespacedName{Name: req.Name}, projectquota); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	if err := r.SyncProjectQuotaStatus(ctx, logger, projectquota); err != nil {
		if k8serrors.IsConflict(err) {
			logger.Info("Update status conflict requeue")
			return ctrl.Result{RequeueAfter: 5 * time.Second}, nil
		}
		logger.Error(err, "update status failed")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *ProjectQuotaReconciler) SyncProjectQuotaStatus(ctx context.Context, logger logr.Logger, projectquota *authv1.ProjectQuota) error {
	originProjectQuota := projectquota.DeepCopy()

	sel, err := labels.Parse(fmt.Sprintf("%s=%s", r.getProjectNameLabel(), projectquota.Name))
	if err != nil {
		return err
	}
	namespaces := &corev1.NamespaceList{}
	if err := r.Client.List(ctx, namespaces, client.MatchingLabelsSelector{Selector: sel}); err != nil {
		return err
	}
	resourcequotas := []corev1.ResourceQuota{}
	for _, namespace := range namespaces.Items {
		quotas := &corev1.ResourceQuotaList{}
		if err := r.Client.List(ctx, quotas, client.InNamespace(namespace.Name)); err != nil {
			return err
		}
		resourcequotas = append(resourcequotas, quotas.Items...)
	}
	sumStatus := corev1.ResourceQuotaStatus{}
	sumStatus.Hard = corev1.ResourceList{}
	sumStatus.Used = corev1.ResourceList{}
	for _, resourcequota := range resourcequotas {
		sumStatus.Hard = quota.Add(sumStatus.Hard, resourcequota.Status.Hard)
		sumStatus.Used = quota.Add(sumStatus.Used, resourcequota.Status.Used)
	}
	projectquota.Status = sumStatus
	if err := r.Client.Status().Patch(ctx, projectquota, client.MergeFrom(originProjectQuota)); err != nil {
		logger.Error(err, "patch status failed")
		return err
	}
	logger.V(5).Info("Update status", "status", projectquota.Status)
	return nil
}

func projectQuotaDeleteFunc(e event.DeleteEvent) bool {
	return false
}

func projectQuotaCreateFunc(e event.CreateEvent) bool {
	return true
}

func projectQuotaUpdateFunc(e event.UpdateEvent) bool {
	return true
}

func projectQuotaGenericFunc(e event.GenericEvent) bool {
	return true
}

func (r *ProjectQuotaReconciler) getProjectNameLabel() string {
	return fmt.Sprintf("%s/project", r.Domain)
}

func (r *ProjectQuotaReconciler) SetupWithManager(mgr ctrl.Manager) error {
	p := predicate.Funcs{
		DeleteFunc:  projectQuotaDeleteFunc,
		CreateFunc:  projectQuotaCreateFunc,
		UpdateFunc:  projectQuotaUpdateFunc,
		GenericFunc: projectQuotaGenericFunc,
	}
	resourceQuotaMapFn := handler.ToRequestsFunc(
		func(rq handler.MapObject) []reconcile.Request {
			ns := &corev1.Namespace{}
			if err := r.Client.Get(context.TODO(), types.NamespacedName{Name: rq.Meta.GetNamespace()}, ns); err != nil {
				r.Log.WithValues("namespace", rq.Meta.GetNamespace()).Error(err, "get namespace failed")
				return nil
			}
			projectName := ns.GetLabels()[r.getProjectNameLabel()]
			if projectName == "" {
				return nil
			}
			resourceQuotaInfo := fmt.Sprintf("%s/%s", rq.Meta.GetNamespace(), rq.Meta.GetName())
			r.Log.Info("Enqueue projectquota for resourcequota", "projectquota", projectName, "resourcequota", resourceQuotaInfo)
			return []reconcile.Request{
				{NamespacedName: types.NamespacedName{
					Name: projectName,
				}},
			}
		})
	return ctrl.NewControllerManagedBy(mgr).
		For(&authv1.ProjectQuota{}).
		Watches(&source.Kind{Type: &corev1.ResourceQuota{}},
			&handler.EnqueueRequestsFromMapFunc{
				ToRequests: resourceQuotaMapFn,
			}).
		WithEventFilter(p).
		WithOptions(controller.Options{
			MaxConcurrentReconciles: r.Concurrency,
		}).
		Complete(r)
}
