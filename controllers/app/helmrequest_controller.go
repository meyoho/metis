/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"alauda.io/metis/pkg/application"
	"alauda.io/metis/pkg/config"
	"context"
	"errors"
	"fmt"
	"github.com/alauda/component-base/compress"
	"github.com/alauda/component-base/resource"
	"github.com/go-logr/logr"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"strings"

	appv1beta1 "alauda.io/metis/apis/app/v1beta1"
	appv1alpha1 "github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
)

// HelmRequestReconciler reconciles a HelmRequest object
type HelmRequestReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=app.alauda.io,resources=helmrequests,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=app.alauda.io,resources=helmrequests/status,verbs=get;update;patch

func (r *HelmRequestReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	log := r.Log.WithValues("helmrequest", req.NamespacedName)

	// your logic here

	var hr appv1alpha1.HelmRequest
	if err := r.Get(ctx, req.NamespacedName, &hr); err != nil {
		log.Error(err, "unable to fetch helmrequest")
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, ignoreNotFound(err)
	}

	if hr.Status.Phase != appv1alpha1.HelmRequestSynced {
		log.Info("helmrequest not in synced status, skip processing.", "name", req.NamespacedName)
		return ctrl.Result{}, nil
	}

	key := fmt.Sprintf("helmrequests.%s/deploy-application", config.BaseDomain)
	if hr.GetAnnotations() != nil && hr.GetAnnotations()[key] == "true" {
		log.Info("found helmrequest enabled application deploy")
	} else {
		log.Info("found helmrequest does not enable deploying application")
		return ctrl.Result{}, nil
	}

	name := hr.GetName()
	ns := hr.GetNamespace()

	// found release
	releaseName := hr.Spec.ReleaseName
	if releaseName == "" {
		releaseName = name
	}

	releaseNS := ns
	if hr.Spec.Namespace != "" {
		releaseNS = hr.Spec.Namespace
	}

	release, err := r.getRelease(releaseName, releaseNS, ctx)
	if err != nil || release.GetName() == "" {
		err = errors.New("cannot found a valid release for helmrequest")
		log.Error(err, "")
		return ctrl.Result{}, err
	}
	log.Info("found deployed release", "name", release.GetName())

	// parse release content and create application
	var app appv1beta1.Application
	found := false

	manifest, err := compress.DecodeToString(release.Spec.ManifestData)
	if err != nil {
		log.Error(err, "unable to decode manifest data of release")
		return ctrl.Result{}, err
	}

	rs, err := resource.ParseFromManifest(manifest)
	if err != nil {
		log.Error(err, "parse resource list from manifest error")
		return ctrl.Result{}, err
	}

	if err := r.Get(ctx, client.ObjectKey{Name: name, Namespace: ns}, &app); err != nil {
		if !isNotFound(err) {
			return ctrl.Result{}, err
		}
	} else {
		log.Info("Application already exist", "name", app.Name)
		if app.GetLabels() == nil || app.GetLabels()["sync-from-helmrequest"] != "true" {
			log.Info("found application but does not looks like it is synced from helmrequest")
			return ctrl.Result{}, nil

		}
		found = true
	}

	if !found {
		log.Info("Application not exist for release, create it ")
		app = *application.NewApplication(name, ns, release.GetName(), rs)
		//app.Annotations["helmrequest.name"] = fmt.Sprintf("%s.%s", name, ns)
		app.OwnerReferences = application.NewHelmRequestOwnerRef(name, hr.GetUID())
		if err := r.Create(ctx, &app); err != nil {
			log.Error(err, "create application error")
			return ctrl.Result{}, err
		}
	}

	// processing sub resources
	for _, item := range rs {
		res := item
		found := false
		// this is buggy code ,we need a way to find the ns of all resource
		if err := r.Get(ctx, client.ObjectKey{Namespace: hr.Spec.Namespace, Name: item.GetName()}, &res); err != nil {
			if strings.Contains(err.Error(), "the server could not find the requested resource") {
				log.Info("we may encountered a namespace error in controller-runtime, retry again", "name", getPrintName(&res))
				if err := r.Get(ctx, client.ObjectKey{Namespace: item.GetNamespace(), Name: item.GetName()}, &res); err != nil {
					log.Error(err, "get resource error", "name", getPrintName(&res))
					return ctrl.Result{}, err
				} else {
					found = true
				}

			} else {
				log.Error(err, "get resource error", "name", getPrintName(&res))
				return ctrl.Result{}, err
			}
		} else {
			found = true
		}

		if found {
			labels := res.GetLabels()
			if res.GetLabels() != nil {
				if isResourceBelongToApp(&res) {
					log.Info("resource already adopted by application, skip", "name", getPrintName(&res))
					continue
				}
			} else {
				labels = map[string]string{}
			}
			// var returnedValue unstructured.Unstructured
			mp := client.MergeFrom(res.DeepCopy())
			labels["app.alauda.io/name"] = fmt.Sprintf("%s.%s", name, ns)
			labels["app.alauda.io/uuid"] = app.GetLabels()["app.alauda.io/uuid"]
			res.SetLabels(labels)
			res.SetOwnerReferences(application.NewApplicationOwnerRef(name, app.GetUID()))

			// if this is a workload
			result, ok, _ := unstructured.NestedStringMap(res.Object, "spec", "template", "metadata", "labels")
			if ok {
				log.Info("this resource contains pod template, may be a pod controller", "name", getPrintName(&res))
				result["app.alauda.io/name"] = labels["app.alauda.io/name"]
				if err := unstructured.SetNestedStringMap(res.Object, result, "spec", "template", "metadata", "labels"); err != nil {
					log.Error(err, "set spec.template.metadata.labels error for resource", "name", getPrintName(&res))
					return ctrl.Result{}, err
				}
			}

			if err := r.Patch(ctx, &res, mp); err != nil {
				log.Error(err, "patch resource error")
				return ctrl.Result{}, err
			} else {
				log.Info("patch resource done", "name", getPrintName(&res))
			}
		}
	}

	// set owner for helmrequest, this is a loop now.

	mp := client.MergeFrom(hr.DeepCopy())
	hr.SetOwnerReferences(application.NewApplicationOwnerRef(name, app.GetUID()))
	if err := r.Patch(ctx, &hr, mp); err != nil {
		log.Error(err, "patch helmrequest for owner ref error")
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil

}

func (r *HelmRequestReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&appv1alpha1.HelmRequest{}).
		Complete(r)
}

func (r *HelmRequestReconciler) getRelease(name, ns string, ctx context.Context) (*appv1alpha1.Release, error) {
	var rl appv1alpha1.ReleaseList
	var rel appv1alpha1.Release
	opts := client.InNamespace(ns)

	if err := r.List(ctx, &rl, opts); err != nil {
		return nil, err
	}

	for _, item := range rl.Items {
		if strings.HasPrefix(item.GetName(), name+".") && item.Status.Status == "deployed" {
			rel = item
			break
		}
	}

	return &rel, nil

}

func isNotFound(err error) bool {
	return apierrs.IsNotFound(err)
}

func ignoreNotFound(err error) error {
	if isNotFound(err) {
		return nil
	}
	return err
}

func getPrintName(res *unstructured.Unstructured) string {
	gvk := res.GroupVersionKind().String()
	return gvk + ", Name=" + res.GetName() + ", Namespace=" + res.GetNamespace()
}

func isResourceBelongToApp(res *unstructured.Unstructured) bool {
	if res.GetLabels()["app.alauda.io/name"] == "" {
		return false
	}
	if res.GetLabels()["app.alauda.io/uuid"] == "" {
		return false
	}

	if res.GetOwnerReferences() == nil {
		return false
	}

	if res.GetOwnerReferences()[0].Kind != "Application" {
		return false
	}
	return true

}
