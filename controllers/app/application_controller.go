/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"alauda.io/metis/pkg/workload"
	"context"
	"fmt"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
	"time"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	appv1beta1 "alauda.io/metis/apis/app/v1beta1"
)

// ApplicationReconciler reconciles a Application object
type ApplicationReconciler struct {
	client.Client
	Log         logr.Logger
	Scheme      *runtime.Scheme
	Concurrency int
}

// +kubebuilder:rbac:groups=app.k8s.io,resources=applications,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=app.k8s.io,resources=applications/status,verbs=get;update;patch

func (r *ApplicationReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	logger := r.Log.WithValues("application", req.NamespacedName)
	st := time.Now()
	logger.Info("Starting reconcile")
	defer logger.Info("Finish reconcile", "cost", time.Since(st))

	application := &appv1beta1.Application{}
	if err := r.Client.Get(ctx, types.NamespacedName{Namespace: req.Namespace, Name: req.Name}, application); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	if err := r.fullyUpdateApplicationStatus(ctx, logger, application); err != nil {
		if k8serrors.IsConflict(err) {
			logger.Info("Update status conflict requeue")
			return ctrl.Result{RequeueAfter: 5 * time.Second}, nil
		}
		logger.Error(err, "update status failed")
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *ApplicationReconciler) fullyUpdateApplicationStatus(ctx context.Context, logger logr.Logger, application *appv1beta1.Application) error {
	originApplication := application.DeepCopy()

	workloadDeploymentsCnt := 0
	workloadStatefulsetsCnt := 0
	workloadDaemonsetsCnt := 0
	workloadsStatus := appv1beta1.WorkloadsStatus{
		Workloads: []appv1beta1.Workload{},
	}
	usedUID := map[types.UID]bool{}
	for _, gk := range application.Spec.ComponentGroupKinds {
		if workload.IsPodController(gk.Kind) {
			sel, err := metav1.LabelSelectorAsSelector(application.Spec.Selector)
			if err != nil {
				logger.Error(err, "LabelSelectorAsSelector failed")
				continue
			}
			switch gk.Kind {
			case workload.DeploymentKind:
				deployments := &appsv1.DeploymentList{}
				if err := r.Client.List(ctx, deployments, client.MatchingLabelsSelector{Selector: sel}); err != nil {
					logger.Error(err, "list workload failed", "kind", gk.Kind, "namespace", application.GetNamespace(), "sel", sel)
				} else {
					for _, deployment := range deployments.Items {
						if _, ok := usedUID[deployment.GetUID()]; ok {
							continue
						}
						usedUID[deployment.GetUID()] = true

						workloadDeploymentsCnt++
						workloadStatus := workload.CalculateDeploymentStatus(&deployment)
						switch workloadStatus.Status {
						case appv1beta1.WLRunning:
							workloadsStatus.Ready++
						case appv1beta1.WLPending:
							workloadsStatus.Pending++
						case appv1beta1.WLStopped:
							workloadsStatus.Stopped++
						}
						workloadsStatus.Workloads = append(workloadsStatus.Workloads, *workloadStatus)
					}
				}
			case workload.StatefulSetKind:
				statefulsets := &appsv1.StatefulSetList{}
				if err := r.Client.List(ctx, statefulsets, client.MatchingLabelsSelector{Selector: sel}); err != nil {
					logger.Error(err, "list workload failed", "kind", gk.Kind, "namespace", application.GetNamespace(), "sel", sel)
				} else {
					for _, statefulset := range statefulsets.Items {
						if _, ok := usedUID[statefulset.GetUID()]; ok {
							continue
						}
						usedUID[statefulset.GetUID()] = true

						workloadStatefulsetsCnt++
						workloadStatus := workload.CalculateStatefulsetStatus(&statefulset)
						switch workloadStatus.Status {
						case appv1beta1.WLRunning:
							workloadsStatus.Ready++
						case appv1beta1.WLPending:
							workloadsStatus.Pending++
						case appv1beta1.WLStopped:
							workloadsStatus.Stopped++
						}
						workloadsStatus.Workloads = append(workloadsStatus.Workloads, *workloadStatus)
					}
				}
			case workload.DaemonSetKind:
				daemonsets := &appsv1.DaemonSetList{}
				if err := r.Client.List(ctx, daemonsets, client.MatchingLabelsSelector{Selector: sel}); err != nil {
					logger.Error(err, "list workload failed", "kind", gk.Kind, "namespace", application.GetNamespace(), "sel", sel)
				} else {
					for _, daemonset := range daemonsets.Items {
						if _, ok := usedUID[daemonset.GetUID()]; ok {
							continue
						}
						usedUID[daemonset.GetUID()] = true

						workloadDaemonsetsCnt++
						workloadStatus := workload.CalculateDaemonsetStatus(&daemonset)
						switch workloadStatus.Status {
						case appv1beta1.WLRunning:
							workloadsStatus.Ready++
						case appv1beta1.WLPending:
							workloadsStatus.Pending++
						case appv1beta1.WLStopped:
							workloadsStatus.Stopped++
						}
						workloadsStatus.Workloads = append(workloadsStatus.Workloads, *workloadStatus)
					}
				}
			}
		}
	}

	application.Status.TotalComponents = workloadDeploymentsCnt + workloadStatefulsetsCnt + workloadDaemonsetsCnt
	application.Status.WorkloadsStatus = workloadsStatus
	r.resolveApplicationState(application, workloadsStatus)

	if err := r.Client.Status().Patch(ctx, application, client.MergeFrom(originApplication)); err != nil {
		logger.Error(err, "patch status failed")
		return err
	}
	logger.Info("Update status", "state", application.Status.State)
	logger.V(5).Info("Update status", "status", application.Status)
	return nil
}

func (r *ApplicationReconciler) resolveApplicationState(application *appv1beta1.Application, workloadsStatus appv1beta1.WorkloadsStatus) {
	switch {
	case application.Spec.AssemblyPhase == appv1beta1.Failed:
		application.Status.State = appv1beta1.WLFailed
	case application.Spec.AssemblyPhase == appv1beta1.Pending:
		application.Status.State = appv1beta1.WLPending
	case application.Status.TotalComponents == 0:
		application.Status.State = appv1beta1.WLEmpty
		application.Status.RemoveAllConditions()
		application.Status.SetCondition(
			appv1beta1.ConditionType(appv1beta1.WLEmpty),
			string(appv1beta1.WLEmpty),
			"No workloads")
	case application.Status.TotalComponents == workloadsStatus.Ready:
		application.Status.State = appv1beta1.WLRunning
		application.Status.RemoveAllConditions()
		application.Status.SetCondition(
			appv1beta1.ConditionType(appv1beta1.WLRunning),
			string(appv1beta1.WLRunning),
			"All workloads are running")
	case application.Status.TotalComponents == workloadsStatus.Stopped:
		application.Status.State = appv1beta1.WLStopped
		application.Status.RemoveAllConditions()
		application.Status.SetCondition(
			appv1beta1.ConditionType(appv1beta1.WLStopped),
			string(appv1beta1.WLStopped),
			"All workloads are stopped")
	case application.Status.TotalComponents == workloadsStatus.Ready+workloadsStatus.Stopped &&
		workloadsStatus.Stopped > 0:
		application.Status.State = appv1beta1.WLPartialRunning
		application.Status.RemoveAllConditions()
		application.Status.SetCondition(
			appv1beta1.ConditionType(appv1beta1.WLPartialRunning),
			string(appv1beta1.WLPartialRunning),
			"Some workloads are running, the others are stopped")
	default:
		application.Status.State = appv1beta1.WLPending
		application.Status.RemoveAllConditions()
		application.Status.SetCondition(
			appv1beta1.ConditionType(appv1beta1.WLPending),
			string(appv1beta1.WLPending),
			"Some workloads are pending")
	}
}

func applicationDeleteFunc(e event.DeleteEvent) bool {
	return false
}

func applicationCreateFunc(e event.CreateEvent) bool {
	return true
}

func applicationUpdateFunc(e event.UpdateEvent) bool {
	return true
}

func applicationGenericFunc(e event.GenericEvent) bool {
	return true
}

// TODO: Add back set ownerreference
func (r *ApplicationReconciler) SetupWithManager(mgr ctrl.Manager) error {
	p := predicate.Funcs{
		DeleteFunc:  applicationDeleteFunc,
		CreateFunc:  applicationCreateFunc,
		UpdateFunc:  applicationUpdateFunc,
		GenericFunc: applicationGenericFunc,
	}
	workloadMapFn := handler.ToRequestsFunc(
		func(wl handler.MapObject) []reconcile.Request {
			apps := &appv1beta1.ApplicationList{}
			wlSet := labels.Set(wl.Meta.GetLabels())
			if err := r.Client.List(context.TODO(), apps, client.InNamespace(wl.Meta.GetNamespace())); err != nil {
				r.Log.WithValues("namespace", wl.Meta.GetNamespace()).Error(err, "list application failed")
				return nil
			}
			if len(apps.Items) == 0 {
				return nil
			}
			for _, app := range apps.Items {
				appSel := labels.Set(app.Spec.Selector.MatchLabels).AsSelector()
				if appSel.Matches(wlSet) {
					appInfo := fmt.Sprintf("%s/%s", app.GetNamespace(), app.GetName())
					wlInfo := fmt.Sprintf("Kind: %s %s/%s", wl.Object.GetObjectKind().GroupVersionKind().Kind, wl.Meta.GetNamespace(), wl.Meta.GetName())
					r.Log.Info("Enqueue app for workload ", "application", appInfo, "workload", wlInfo)
					return []reconcile.Request{
						{NamespacedName: types.NamespacedName{
							Name:      app.GetName(),
							Namespace: app.GetNamespace(),
						}},
					}
				}
			}
			return nil
		})
	return ctrl.NewControllerManagedBy(mgr).
		For(&appv1beta1.Application{}).
		Watches(&source.Kind{Type: &appsv1.Deployment{}},
			&handler.EnqueueRequestsFromMapFunc{
				ToRequests: workloadMapFn,
			}).
		Watches(&source.Kind{Type: &appsv1.DaemonSet{}},
			&handler.EnqueueRequestsFromMapFunc{
				ToRequests: workloadMapFn,
			}).
		Watches(&source.Kind{Type: &appsv1.StatefulSet{}},
			&handler.EnqueueRequestsFromMapFunc{
				ToRequests: workloadMapFn,
			}).
		WithEventFilter(p).
		WithOptions(controller.Options{
			MaxConcurrentReconciles: r.Concurrency,
		}).
		Complete(r)
}
