/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"io/ioutil"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/klog"
	"os"

	"alauda.io/metis/pkg/config"
	"alauda.io/metis/pkg/webhooks"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	appv1beta1 "alauda.io/metis/apis/app/v1beta1"
	authv1 "alauda.io/metis/apis/auth/v1"
	appcontroller "alauda.io/metis/controllers/app"
	authcontroller "alauda.io/metis/controllers/auth"
	"alauda.io/metis/pkg/runnables"
	appv1alpha1 "github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	// +kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = authv1.AddToScheme(scheme)
	_ = appv1beta1.AddToScheme(scheme)
	_ = appv1alpha1.AddToScheme(scheme)
	// +kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager.")
	flag.IntVar(&config.QPS, "qps", 100, "base domain to be used in labels/annotations")
	flag.IntVar(&config.Burst, "burst", 200, "base domain to be used in labels/annotations")
	flag.StringVar(&config.BaseDomain, "base-domain", "alauda.io", "base domain to be used in labels/annotations")
	flag.IntVar(&config.ProjectWorkers, "project-workers", 10, "workers for project controller")
	flag.IntVar(&config.ApplicationWorkers, "application-workers", 30, "workers for application controller")
	flag.Int64Var(&config.OwnerSetInterval, "owner-set-interval", 5*60, "The interval seconds to set owner references for all applications.")
	klog.InitFlags(nil)
	flag.Parse()

	ctrl.SetLogger(zap.New(func(o *zap.Options) {
		o.Development = true
	}))

	restCfg := ctrl.GetConfigOrDie()
	restCfg.QPS = float32(config.QPS)
	restCfg.Burst = config.QPS
	// setupLog.Info("rest config is", "cfg", mgr.GetConfig())
	config.RestConfig = *restCfg

	mgr, err := ctrl.NewManager(restCfg, ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		LeaderElection:     enableLeaderElection,
		LeaderElectionID:   "metis-controller-lock",
		Port:               9443,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	err = (&appcontroller.HelmRequestReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("HelmRequest"),
	}).SetupWithManager(mgr)
	if err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "HelmRequest")
		os.Exit(1)
	}
	if err = (&appcontroller.ApplicationReconciler{
		Client:      mgr.GetClient(),
		Log:         ctrl.Log.WithName("controllers").WithName("Application"),
		Scheme:      mgr.GetScheme(),
		Concurrency: config.ApplicationWorkers,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Application")
		os.Exit(1)
	}
	if err = (&authcontroller.ProjectQuotaReconciler{
		Client:      mgr.GetClient(),
		Log:         ctrl.Log.WithName("controllers").WithName("ProjectQuota"),
		Scheme:      mgr.GetScheme(),
		Domain:      config.BaseDomain,
		Concurrency: config.ProjectWorkers,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ProjectQuota")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	// read caBundle from secret

	c := kubernetes.NewForConfigOrDie(restCfg)
	s, err := c.CoreV1().Secrets(os.Getenv("KUBERNETES_NAMESPACE")).Get("metis-webhook-cert", metav1.GetOptions{})
	if err != nil {
		setupLog.Error(err, "read secret data error")
		os.Exit(1)
	}

	// create webhook configuration
	if err := webhooks.CreateMutateWebhook(os.Getenv("KUBERNETES_NAMESPACE"), s.Data["caBundle"], mgr); err != nil {
		setupLog.Error(err, "create webhook config error")
		os.Exit(1)
	}

	if err := createCerts(s); err != nil {
		setupLog.Error(err, "create webhook cert files error")
		os.Exit(1)
	}

	// register webhooks
	webhooks.RegisterWebhooks(mgr)
	if err := runnables.CheckResourceRatio(); err != nil {
		setupLog.Error(err, "check resource ratio error:")
		os.Exit(1)
	}
	applicationOwnerReferenceController, err := runnables.NewApplicationOwnerReferenceController(mgr, config.OwnerSetInterval)
	if err != nil {
		setupLog.Error(err, "create applicationOwnerReferenceController failed")
		os.Exit(1)
	}
	mgr.Add(applicationOwnerReferenceController)

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		if err := webhooks.DeleteWebhook(mgr); err != nil {
			setupLog.Error(err, "delete webhook config error")
		}
		os.Exit(1)
	}

	if err := webhooks.DeleteWebhook(mgr); err != nil {
		setupLog.Error(err, "delete webhook config error")
		os.Exit(1)
	}

}

func createCerts(s *corev1.Secret) error {

	path := "/tmp/k8s-webhook-server/serving-certs"

	if err := os.MkdirAll(path, 0755); err != nil {
		return err
	}

	setupLog.Info("Create serving-certs dir")

	if err := ioutil.WriteFile(path+"/"+"tls.crt", s.Data["tls.crt"], 0420); err != nil {
		return err

	}

	if err := ioutil.WriteFile(path+"/"+"tls.key", s.Data["tls.key"], 0420); err != nil {
		return err

	}

	return nil

}
